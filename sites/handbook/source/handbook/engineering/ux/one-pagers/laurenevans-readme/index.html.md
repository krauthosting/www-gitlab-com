---
layout: markdown_page
title: "Lauren Evans's README"
---



## Lauren Evan's README
Hi, I am Lauren Evans and I am a UX Research Manager at GitLab. I joined the team in November 2021.

Why I did this : I put this together because I think in order for teams to be highly functioning, there needs to be a culture of psychological safety (trust). I think a large part of feeling safe in a team is knowing what to expect from your leaders - and rather than have everyone go through a trial and error process of figuring out how to work with me, I’m giving you a cheat sheet! Like me, this is a work in progress.  

## About me

- I am a mom and an animal lover (especially horses). I currently live in Helena, MT on a small farm with horses, dogs, cats, and humans! I have been in Montana the last 8 years. Prior, I lived in Western Washington where I was born and raised. 
- I have spent my whole career in software development, specifically UX. I love all things UX research and love to help individuals and teams be data informed throughout the product development lifecycle. 
- One quote I live by: _"There is no growth in the comfort zone and no comfort in the growth zone."_ 

## My role as a UX Research Manager
- I am super excited to be managing this team of amazing UX researchers. 
- From [the Manager, UX Research job description](https://about.gitlab.com/job-families/engineering/ux-research-manager/): Managers in the UX Research department at GitLab see the team as their product. While they are credible as researchers and know the details of what UX Researchers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of UX Research commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.


## My leadership style
- I am an [advocate INFJ-T](https://www.16personalities.com/infj-personality) and this description matches well with how I tend to lead: _As managers, Advocates may dislike wielding their power. These personalities prefer to see those who work under them as equals. Rather than micromanage their subordinates, Advocates often prefer to empower them to think and act independently. They work hard to encourage others, not to crack the whip._
- People first! I am here for you and want the best for you. Your success and development is my primary focus. I am here to get you the gold stars… not collect them myself. 
- Trust is huge! I want you to trust me and I default to trusting you until I have a reason to not. 
- I try to be as transparent as possible. Even when it’s hard. 
-I will tell you if I don’t know something but I will do my best to get you answers. This often comes off like I don’t know what I am doing… which at times I don’t! But I will hunt deeply for answers. 
-I really don’t like micromanaging… so I tend to default to the other end of the spectrum. I work to be aware of and use situational leadership to recognize when someone needs more hands on.. But don’t hesitate to ask or tell me what you need. 
-I am EAGER for feedback. Whether it be general or specific to a project or idea I have. I want your input. I like feedback in any form, slack, email, video chat, whatever you like. I will continually ask for your input on various things and feedback on myself. Also, I am extremely flexible. If you have an idea of something to change or try I am usually game. 


## What I value
- Punctuality and always try to be on time. It makes me feel bad if I am not on time. However, I don’t criticize or judge those that are late.  
- Taking initiative! Further, when it comes to problems. I tend to want to take action towards a solution vs dwelling too long on the problem.
- I like to have FUN! I like to joke, goof off, or just share a good laugh. Work doesn’t have to be boring. 
- Work life balance is very important to me. I will set boundaries and working hours. I will not slack you outside of work hours if at all possible. I encourage you to do the same. 



## What motivates me
- Seeing you develop! 
- You getting recognition for your work 
- Seeing the impact of UX research on the business and ultimately for the user
- Developing a high functioning team
- Having fun! 



## Known failure modes
- Sometimes I leap too quickly on half baked ideas. Call me out on this. 
- I can not go too deep into explanations or expectations. A good way to ground me is to ask “what does done look like?” 
- I need to be firmer. I tend to be too nice and tolerate things that I shouldn’t. 



## Logistics
- My working hours are 8-4 MST, give or take an hour.
- If my calendar has free space, go ahead and book it. I am free to chat any time! If there is not time that works for the both of us, ask, I can usually shift things around. I am NEVER too busy for you. 
- You don’t need to schedule time to talk to me ahead of time. Just Slack me and I am always willing to talk either asynchronous or synchronous.

